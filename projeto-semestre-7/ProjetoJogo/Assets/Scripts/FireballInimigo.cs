﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballInimigo : MonoBehaviour {

	public Player player;

	void OnTriggerEnter (Collider objeto) {

		if (objeto.tag == "Player") {

			player = objeto.GetComponent<Player>();
			player.vida -= 1;

			if (player.vida <= 0) {
				Time.timeScale = 0;
			}
			Debug.Log("Vida Player" + player.vida);

			Destroy(gameObject);
		}
	}
	
}
