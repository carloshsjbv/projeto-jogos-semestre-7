﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public float vida=10;

	public float forcaTiro=100;
	public Rigidbody FireballPlayer;
	public GameObject CanoDaArmaPlayer;
	//public GameObject RoboInimigo;


	// Update is called once per frame
	void Update () {

		if (Input.GetButton("Fire1")) 
		{
			Rigidbody instance = Instantiate(FireballPlayer, CanoDaArmaPlayer.transform.position, CanoDaArmaPlayer.transform.rotation);
			Vector3 frente = CanoDaArmaPlayer.transform.TransformDirection(Vector3.forward);

			instance.AddForce(frente * forcaTiro);
		}

	}


	public void Dano(float valorDano) {

		vida -= valorDano;
		if ( vida <= 0){
			Destroy(gameObject);
		}

	}

}