﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    public List<BaseWeapon> weapons;
    public float vida = 10;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        InputPlayer();
    }

    public void InputPlayer()
    {
        if ((Input.GetButton("Fire1") && weapons[0].isAutomatic) || Input.GetButtonDown("Fire1"))
        {
            weapons[0].Fire();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            weapons[0].Reload();
        }

        if (Input.GetButton("Fire2"))
        {
            weapons[0].EnableZoom();
        }
        else
        {
            weapons[0].DisableZoom();
        }        
    }

    public void Dano(float valorDano)
    {

        vida -= valorDano;
        if (vida <= 0)
        {
            Destroy(gameObject);
        }

    }
}
