﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoboInimigo : MonoBehaviour {

	// Controle da vida
	public float vidaInimigo = 1;


	//Controladores RoboInimigo: 
	public float TempoDeTiro;
	public float distancia;
	public Rigidbody FireballInimigo;
	public GameObject CanoDaArma;

	public GameObject Player;
	public float Velocidade = 100;


	// Use this for initialization
	void Start () 
	{
		Player = GameObject.FindWithTag("Player");
	}

	void FixedUpdate()
	{

		float distancia = Vector3.Distance(transform.position, Player.transform.position);

		TempoDeTiro += Time.deltaTime;

		olhar();

		if(distancia > 40)
		{
			seguir();
		} else {

			if(TempoDeTiro>0.9) 
			{
				Instantiate(FireballInimigo, CanoDaArma.transform.position, CanoDaArma.transform.rotation);
				TempoDeTiro = 0;
			}
			GetComponent<Animator>().SetBool("MovendoParaFrente", false);

		}

	}


	void olhar() {
		Quaternion visao;
		visao = Quaternion.LookRotation(Player.transform.position - transform.position); // Pra onde olhar
		transform.rotation = Quaternion.Slerp(transform.rotation, visao, 6f * Time.deltaTime); //Como olhar
	}

	void seguir() {
		transform.Translate(transform.forward * -2f * Time.deltaTime);
		GetComponent<Animator>().SetBool("MovendoParaFrente", true);
	}


	// Calculo de dados 
	public void dano(float quantidadeDano){

		vidaInimigo -= quantidadeDano;
		if (vidaInimigo <= 0){
			Destroy(gameObject);
		}

	}
}
