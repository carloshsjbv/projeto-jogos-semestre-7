﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballPlayer : MonoBehaviour {

	public RoboInimigo inimigo;

	void OnTriggerEnter (Collider objeto) {

		if (objeto.tag == "Inimigo") {

			inimigo = objeto.GetComponent<RoboInimigo>();
			inimigo.vidaInimigo -= 1;

			if (inimigo.vidaInimigo <= 0) {
				//Destroi inimigo
				Destroy(objeto.gameObject);
			}

			Debug.Log("Vida Inimigo" + inimigo.vidaInimigo);
		}

		// Destroy fireball
		Destroy(gameObject);

	}

}
