﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseWeapon : MonoBehaviour
{

    public float damage;
    public float range;
    public float fireRateTime;
    public int capacityAmmo;
    public int capacityAmmoOutWeapon;
    public Transform positionSpawnProjetctil;
    public ParticleSystem fireParticle;
    public bool isAutomatic;
    public float aimZoom;

    private int currentAmountAmmo;
    private int currentAmountAmmoOutWeapon;
    private float currentfireRateTime;
    private bool canFire = true;
    private float startZoom;
    private ParticleSystem currentFireParticle;

    public BulletBehaviour bullet;
    public GameObject decalShotPrefab;

    // Use this for initialization
    protected void Start()
    {
        currentAmountAmmo = capacityAmmo;
        currentAmountAmmoOutWeapon = capacityAmmoOutWeapon;
        startZoom = Camera.main.fieldOfView;
        GameObject tempParticle = Instantiate(fireParticle.gameObject, positionSpawnProjetctil.position, transform.rotation);
        currentFireParticle = tempParticle.GetComponent<ParticleSystem>();
        currentFireParticle.transform.SetParent(positionSpawnProjetctil);
    }

    // Update is called once per frame
    protected void Update()
    {
        currentfireRateTime += Time.deltaTime;
        if (currentfireRateTime > fireRateTime)
            canFire = true;
        else
            canFire = false;
    }

    public void Fire()
    {
        if (currentAmountAmmo > 0 && canFire)
        {
            Vector3 startPosition = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            OnFire(damage, range, startPosition);
            currentAmountAmmo--;
            currentfireRateTime = 0;
            currentFireParticle.Play();
            WeaponsAnimation("Fire");
        }
    }

    public void Reload()
    {
        OnReload();
        int needAmmo = capacityAmmo - currentAmountAmmo;

        if (currentAmountAmmoOutWeapon > needAmmo)
        {
            if (currentAmountAmmo < capacityAmmo)
            {
                currentAmountAmmoOutWeapon -= needAmmo;
                currentAmountAmmo += needAmmo;
                //if (needAmmo.Equals(capacityAmmo))
                //    WeaponsAnimation("ReloadEmpty");
                //else
                WeaponsAnimation("Reload");
            }
        }
        else
        {
            currentAmountAmmo += currentAmountAmmoOutWeapon;
            currentAmountAmmoOutWeapon = 0;
        }
    }

    public void WeaponsAnimation(string animationName)
    {
        gameObject.GetComponentInChildren<Animation>().Play(animationName);
    }

    public void EnableZoom()
    {
        Camera.main.fieldOfView = aimZoom;
    }

    public void DisableZoom()
    {
        Camera.main.fieldOfView = startZoom;
    }

    protected abstract void OnFire(float damage, float range, Vector3 startPosition);
    protected abstract void OnReload();
}
