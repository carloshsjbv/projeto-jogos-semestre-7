﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssaultRifleBehaviour : BaseWeapon
{
    // Use this for initialization
    new void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    new void Update()
    {
        base.Update();
    }

    protected override void OnFire(float damage, float range, Vector3 startPosition)
    {
        Ray rayShot = new Ray(startPosition, transform.forward);
        RaycastHit hitInfo;
        float ofsset = 0.009f;

        if (Physics.Raycast(rayShot, out hitInfo, range))//out coleta as informações colisão e salva no hitInfo
        {
            //Debug.Log(hitInfo.collider.name);
            //Debug.DrawLine(transform.position, hitInfo.point);
            Quaternion hitRotation = Quaternion.LookRotation(-hitInfo.normal);
            GameObject tempDecal = Instantiate(decalShotPrefab, hitInfo.point + (hitInfo.normal * ofsset), hitRotation);
        }
        Instantiate(bullet.gameObject, positionSpawnProjetctil.position, transform.rotation);
    }

    protected override void OnReload()
    {

    }

}
