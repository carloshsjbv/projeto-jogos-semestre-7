﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{

    public float speed;
    public float timeToLive;

    // Use this for initialization
    void Start()
    {
        Destroy(gameObject, timeToLive);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }


    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Teste" + other);


        if(other.tag == "Inimigo")
        {
            Debug.Log("ASDASDSADSADSDSADSADASDASDASDSADAS");
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
        
    }

}
